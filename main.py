from fastai import *
from fastai.vision import *
import torch
import io
import base64
from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
from information import scn2_progress_information, scn2bim_information

resnet34_model1 = load_learner("./model1")
resnet34_model2 = load_learner("./model2")


def classify(img, mode):
    image_base64 = img.replace("data:image/jpeg;base64,", "")
    image_bytes = io.BytesIO(base64.b64decode(image_base64))
    img = open_image(image_bytes)
    if mode == "SCAN_2BIM":
        predictions = resnet34_model1.predict(img)
        probability = torch.max(predictions[2]).item()
    else:
        predictions = resnet34_model2.predict(img)
        probability = torch.max(predictions[2]).item()
    return [str(predictions[0]), probability]


def get_label_information(label, information):
    for item in information:
        if item["type"].lower() == label.lower():
            return item


def get_information(label_type, label):
    if label_type == "SCAN2BIM":
        return get_label_information(label, scn2bim_information)
    return get_label_information(label, scn2_progress_information)


app = Flask(__name__, static_folder='./build')

CORS(app)


# Serve React App
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')


@app.route('/predict', methods=["POST"])
def prediction():
    print("newMessage")
    json_data = request.get_json()
    img = json_data["img"]
    mode = json_data["mode"]
    predict = classify(img, mode)
    if str(predict[0]) == "Verbindungsbauteile Typ 1":
        return jsonify(
            label="Verbindungsbauteile Typ 2",
            probability=str(predict[1])
        )
    if str(predict[0]) == "Verbindungsbauteile Typ 2":
        return jsonify(
            label="Verbindungsbauteile Typ 1",
            probability=str(predict[1])
        )
    if str(predict[0]) == "Dachhalter":
        return jsonify(
            label="Giebelelement",
            probability=str(predict[1])
        )
    if str(predict[0]) == "Dachplatte":
        return jsonify(
            label="Dachelement",
            probability=str(predict[1])
        )

    return jsonify(
        label=str(predict[0]),
        probability=str(predict[1]),
    )


@app.route('/information')
def information():
    print("information incoming")
    args = request.args
    label = args["label"]
    label_type = args["labelType"]
    print(label, label_type)
    info = get_information(label_type, label)
    return info


app.run(host='localhost')
