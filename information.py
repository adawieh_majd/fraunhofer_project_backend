scn2bim_information = [
    {
        "type": "3er_bodenplatte",
        "label": "3er Bodenplatte",
        "ID": "1A",
        "Größe": "Länge = 75mm|Breite = 25mm|Höhe/ Tiefe = 5mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 24,
        "Anzahl der Verbinder": 0,
        "Kosten": "5 Euro",
        "Anzahl der Vertiefung/ Einpressung": 3,
        "Zugehörigkeit zu Bauprozess": "ID1",
        "Vorbedingungen für Bauteilmontage": "keine",
        "Besonderheiten": "keine"
    },
    {
        "type": "2er_bodenplatte",
        "label": "2er Bodenplatte",
        "ID": "1B",
        "Größe": "Länge = 50mm|Breite = 25mm|Höhe/ Tiefe = 5mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 16,
        "Anzahl der Verbinder": 0,
        "Kosten": "4 Euro",
        "Anzahl der Vertiefung/ Einpressung": 2,
        "Zugehörigkeit zu Bauprozess": "ID1",
        "Vorbedingungen für Bauteilmontage": "keine",
        "Besonderheiten": "keine"
    },
    {
        "type": "Wandelement",
        "label": "Wandelement",
        "ID": "2A",
        "Größe": "Länge = 50mm|Breite = 25mm|Höhe/ Tiefe = 5mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 0,
        "Anzahl der Verbinder": 4,
        "Kosten": "3,50 Euro",
        "Anzahl der Vertiefung/ Einpressung": 0,
        "Zugehörigkeit zu Bauprozess": "ID2",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B und 1C",
        "Besonderheiten": "Wandseite angeschrägt"
    },
    {
        "type": "Tuerelement",
        "label": "Türelement",
        "ID": "2B",
        "Größe": "Länge = 50mm|Breite = 25mm|Höhe/ Tiefe = 5mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 0,
        "Anzahl der Verbinder": 2,
        "Kosten": "6,50 Euro",
        "Anzahl der Vertiefung/ Einpressung": 0,
        "Zugehörigkeit zu Bauprozess": "ID2",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B und 1C",
        "Besonderheiten": "keine"
    },
    {
        "type": "Fensterelement",
        "label": "Fensterelement",
        "ID": "3A",
        "Größe": "Länge = 50mm|Breite = 25mm|Höhe/ Tiefe = 5mm",
        "Farbe": "transparent",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 0,
        "Anzahl der Verbinder": 8,
        "Kosten": "8 Euro",
        "Anzahl der Vertiefung/ Einpressung": 0,
        "Zugehörigkeit zu Bauprozess": "ID3",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B, 1C, 2A und 2B",
        "Besonderheiten": "keine angeschrägten Seiten"
    },
    {
        "type": "Giebelelement",
        "label": "Giebelelement",
        "ID": "5A",
        "Größe": "Länge = 61mm|Breite = 61mm|Höhe/ Tiefe = 5mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 0,
        "Anzahl der Verbinder": 2,
        "Kosten": "7,50 Euro",
        "Anzahl der Vertiefung/ Einpressung": 0,
        "Zugehörigkeit zu Bauprozess": "ID5",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B, 1C, 2A, 2B, 3A, 4A und 4B",
        "Besonderheiten": "keine"
    },
    {
        "type": "Dachelement",
        "label": "Dachelement",
        "ID": "5B",
        "Größe": "Länge = 75mm|Breite = 17mm|Höhe/ Tiefe = 2mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 0,
        "Anzahl der Verbinder": 2,
        "Kosten": "4,50 Euro",
        "Anzahl der Vertiefung/ Einpressung": 0,
        "Zugehörigkeit zu Bauprozess": "ID5",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B, 1C, 2A, 2B, 3A, 4A, 4B und 5A",
        "Besonderheiten": "keine"
    },
    {
        "type": "Eckbauteil",
        "label": "Eckbauteil",
        "ID": "4A",
        "Größe": "Länge = 12mm|Breite = 12mm|Höhe/ Tiefe = 5mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 2,
        "Anzahl der Verbinder": 0,
        "Kosten": "2 Euro",
        "Anzahl der Vertiefung/ Einpressung": 3,
        "Zugehörigkeit zu Bauprozess": "ID4",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B, 1C, 2A, 2B und 3A",
        "Besonderheiten": "keine"
    },
    {
        "type": "Verbindungsbauteile_Typ_1",
        "label": "Verbindungsbauteile Typ 1",
        "ID": "1C",
        "Größe": "Länge = 16mm|Breite = 8mm|Höhe/ Tiefe = 1mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 0,
        "Anzahl der Verbinder": 4,
        "Kosten": "1,50 Euro",
        "Anzahl der Vertiefung/ Einpressung": 0,
        "Zugehörigkeit zu Bauprozess": "ID1",
        "Vorbedingungen für Bauteilmontage": "1A und 1B",
        "Besonderheiten": "keine"
    },
    {
        "type": "Verbindungsbauteile_Typ_2",
        "label": "Verbindungsbauteile Typ 2",
        "ID": "4B",
        "Größe": "Länge = 25mm|Breite = 5mm|Höhe/ Tiefe = 4mm",
        "Farbe": "weiß",
        "Material": "Kunststoff",
        "Anzahl der Löcher": 2,
        "Anzahl der Verbinder": 0,
        "Kosten": "1 Euro",
        "Anzahl der Vertiefung/ Einpressung": 3,
        "Zugehörigkeit zu Bauprozess": "ID4",
        "Vorbedingungen für Bauteilmontage": "Anbau 1A, 1B, 1C, 2A, 2B und 3A",
        "Besonderheiten": "keine"
    }
]

scn2_progress_information = [
    {
        "type": "Zustand_1",
        "label": "Bauzustand 1",
        "ID Bauprozess": "ID1",
        "Name": "Bauzustand 1",
        "Verbaute Bauteile von Zustand 1": [
            "4 x 3er Bodenplatte",
            "1 x 2er Bodenplatte",
            "18 x Typ2-Verbindungsbauteile"
        ],
        "Insgesamt verbaute Bauteile": 23
    },
    {
        "type": "Zustand_2",
        "label": "Bauzustand 2",
        "ID Bauprozess": "ID2",
        "Name": "Bauzustand2",
        "Verbaute Bauteile von Zustand 2": [
            "8 x Wandelemente (angeschrägt)",
            "1 x Türelement"
        ],
        "Insgesamt verbaute Bauteile": "23 + 9 = 32"
    },
    {
        "type": "Zustand_3",
        "label": "Bauzustand 3",
        "ID Bauprozess": "ID3",
        "Name": "Bauzustand3",
        "Verbaute Bauteile von Zustand 3": [
            "5 x Fensterelemente (nicht angeschrägt)"
        ],
        "Insgesamt verbaute Bauteile": "23 + 9 + 5 = 37"
    },
    {
        "type": "Zustand_4",
        "label": "Bauzustand 4",
        "ID Bauprozess": "ID4",
        "Name": "Bauzustand4",
        "Verbaute Bauteile von Zustand 4": [
            "10 x Typ1-Verbindungselemente",
            "4 x Eckbauteile"
        ],
        "Insgesamt verbaute Bauteile": "23 + 9 + 5 + 14 = 51"
    },
    {
        "type": "Zustand_5",
        "label": "Bauzustand 5",
        "ID Bauprozess": "ID5",
        "Name": "Bauzustand5",
        "Verbaute Bauteile von Zustand 5": [
            "2 x Dachhalterungen",
            "8 x Dachelemente"
        ],
        "Insgesamt verbaute Bauteile": "23 + 9 + 5 + 14 + 10 = 61"
    }
]
